# Python Multithreading Examples

Multithreading powered by Python. These examples are part of a presentation.

## How to use it
This repository contains several examples of how to use multithreading with Python. Each folder refers to a particular example and its content is describe below.

### Example: data_loader
This folder contains two files:
 - data_loader.py, which is the program you shoud run.
 - dl_threading.py, which contains functions related to thread use.

Go inside the folder and type:
```shell
python data_loader.py -h
```

It should provide all the help you need to use this program. The purpose of this example is to show you different technics to achieve the same goal. It cover Executors, Futures, Queues and the low-level Thread class.

### Example: threads
This folder contains to files:
 - simple_thread.py, which is a basic script showing how to use the Thread class.
 - custom_thread.py, which is a more advanced example. Here the Thread class is sub-classed.

### Example: locks
This example is about Locks and RLocks : how to use them, which particularities they have.

### Example: semaphore
Simply shows how to use Semaphore objects.

### Example: condition
Same as Semaphore, but for Condition objects.

### Example: communication
This example shows how to communicate between threads : it covers the Event object and the use of queues.

### Example: barrier
This one is about Barrier objects and how to use them.