# -*- coding: utf-8 -*-
"""This module defines several functions to load the data using threads."""
import os
import contextlib

from threading import Thread
from queue import Queue, Empty
from concurrent.futures import thread, as_completed

import data_loader


def use_executor_and_map(files):
    """
    Load the content of files and display it on the standard output.

    Behind the scene, this function uses a ThreadPoolExecutor with 5 workers and schedules data
    loading using the map function. Results are displayed in the same order as provided in files.
    To load the content of a file, it uses the load_one function.

    :param files: the files containing the data
    """
    with thread.ThreadPoolExecutor(5) as executor:
        results = executor.map(data_loader.load_one, files)

        for result in results:
            print(result, end=' ')


def use_executor_and_futures(files):
    """
    Load the content of files and display it on the standard output.

    Behind the scene, this function uses a ThreadPoolExecutor with
    5 workers and schedules data loading using the submit function.
    Each Future object is then stored and tasks result is displayed
    as soon as possible (and thus can differ from submit order).
    To load the content of a file, it uses the load_one function.

    :param files: the files containing the data
    """
    with thread.ThreadPoolExecutor(5) as executor:
        pending = []
        for file in files:
            pending.append(executor.submit(data_loader.load_one, file))

        for task in as_completed(pending):
            print(task.result(), end=' ')


def _consume_until_empty(queue_in, queue_out):
    """
    Consume elements from the provided queue until this queue is empty.

    Elements are expected to be files containing data that can be extracted using load_one function.

    :param queue_in: the queue to consume
    :param queue_out: where to put results
    """
    with contextlib.suppress(Empty):
        while True:
            file = data_loader.load_one(queue_in.get(block=False))
            queue_out.put(file)
            queue_in.task_done()


def use_threads(files):
    """
    Use the low-level threading module to handle data load.

    This function puts all files in a queue and then a bunch of raw thread consume them and load their data. Because
    printing on the standard output using print function is not thread safe, we also use a Lock to ensure only one
    thread at the time can print its result. Elements are queued at the same order than provided but results are
    displayed as soon as possible.

    :param files: files containing the data
    """
    # Start by filling the queue with the data
    queue_in = Queue(len(files))
    for file in files:
        queue_in.put(file)

    # Create the queue that will contain results
    queue_out = Queue(len(files))

    # Create a bunch of thread and start them
    threads = []
    for i in range(5):
        threads.append(Thread(target=_consume_until_empty, args=(queue_in, queue_out)))
        threads[-1].start()

    # Wait for completion
    for thread in threads:
        thread.join()

    # Ensure queue is empty
    if not queue_in.empty():
        print('Warning : not all data has been processed')

    # print result
    for _ in range(queue_out.qsize()):
        print(queue_out.get(), end=' ')
        queue_out.task_done()
