# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""This module is designed as a base bloc for demonstrating how threading is working in python."""
import os
import time
import random
import string
import functools
import itertools


def generate_data(folder='data'):
    """
    Generate data needed by the application.

    This function will create 2 ** len(string.ascii_uppercase)
    files containing the permutations of three unique uppercased letters.

    :param folder: where the files will be created
    """
    # First, create the folder
    os.mkdir(folder)

    # Then create files
    raw_data = itertools.permutations(string.ascii_uppercase, 2)
    for r_data in raw_data:
        r_data = ''.join(r_data)
        with open(os.path.join(folder, r_data + '.txt'), 'w') as fd:
            fd.writelines(r_data)


def is_data_generated(folder='data'):
    """
    Tell if the data has been generated.

    This function will look if the ``folder`` exists. It should be enough for this application.

    :param folder: the folder that should be checked. Must be the same as the one provided to function
        ``generate_data``.
    """
    return os.path.exists(folder)


def need_data(func, folder='data'):
    """
    Tell the decorated function needs the data to be generated.

    An extra call to ``generate_data`` will be made if the data has not been generated.

    :param folder: where the data should be generated
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if not is_data_generated(folder):
            generate_data(folder)
        return func(*args, **kwargs)
    return wrapper


def load_one(file):
    """
    Load the content of the provided file and return it.

    A delay is introduced to emulate a time consuming operation.
    In fact, this function only reads the first line of a
    text file (but it is enough in our case).

    :param file: file to read
    """
    time.sleep(random.random())
    with open(file, 'r') as fd:
        return fd.readline()


def build_file_list(folder):
    """
    Build the list of files in the provided folder.

    This function behaves like os.listdir, but the returned items will contain the folder's name in addition to the
    file's one. Elements are ordered.

    :param folder: folder where the files are
    """
    return sorted(os.path.join(folder, filename) for filename in os.listdir(folder))


def compute_time(func):
    """
    Decorate a function to calculate the elapsed time.

    This function uses time.perf_counter() to get the best accuracy according to your platform. The returned result is
    a 2-tuple : the first item is the result returned by the function and the second item is the elapsed time.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        rtrn = func(*args, **kwargs)
        return rtrn, time.perf_counter() - start
    return wrapper


def use_sequential_load(files):
    """
    Load data sequentially.

    This function is using sequential load, meaning that only
    one thread is handling the whole data.

    :param files: the list of files to load.
    """
    results = (load_one(f) for f in files)
    for result in results:
        print(result, end=' ')


if __name__ == "__main__":
    import argparse
    import dl_threading

    # First we need to configure the command line parser
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('folder', nargs='?', default='data',
                        help='Where is the data needed for this application. Optional (default is data).')
    OPTIONS = PARSER.add_mutually_exclusive_group()
    OPTIONS.add_argument('-g', '--generate-data', action='store_true',
                         help='Only generate the data needed by the application.')
    OPTIONS.add_argument('-m', '--use-map', action='store_true',
                         help='Load data using a thread pool executor and the map function.')
    OPTIONS.add_argument('-t', '--use-threads', action='store_true',
                         help='Load data using a the low level threading API.')
    OPTIONS.add_argument('-f', '--use-futures', action='store_true',
                         help='Load data using a thread pool executor and handeling Futures manually.')

    # Then we can parse arguments and branch on them
    parsed = PARSER.parse_args()

    # If the user wants to generate the data only, generate and stops
    if parsed.generate_data:
        generate_data(parsed.folder)

    # Otherwise, we are going to use one of the available functions to load the data. Each function is decorated to
    # compute its calculation time and to ensure data has been generated first.
    else:
        data = need_data(build_file_list, parsed.folder)(parsed.folder)
        if parsed.use_map:
            func = need_data(compute_time(
                dl_threading.use_executor_and_map), parsed.folder)
            print("(MAP) Elapsed time: {[1]:3.3f}s".format(func(data)))
        elif parsed.use_futures:
            func = need_data(compute_time(
                dl_threading.use_executor_and_futures), parsed.folder)
            print("(FUT) Elapsed time: {[1]:3.3f}s".format(func(data)))
        elif parsed.use_threads:
            func = need_data(compute_time(dl_threading.use_threads), parsed.folder)
            print("(THR) Elapsed time: {[1]:3.3f}s".format(func(data)))
        else:
            func = need_data(compute_time(use_sequential_load), parsed.folder)
            print("(SEQ) Elapsed time: {[1]:3.3f}s".format(func(data)))
