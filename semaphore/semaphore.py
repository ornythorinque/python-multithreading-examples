# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""Simple script to demonstrate how semaphores work."""
import threading
import time


def use_limited_resource(sem):
    """
    Simulate operation on a limited resource.

    The way the semaphore is used is a bit artificial
    because we want to show that it can be acquired only
    if the internal counter is greater than 0.
    """
    c_thread = threading.current_thread()
    while not sem.acquire(blocking=False):
        print("{.name}: resource not available.".format(c_thread))
        time.sleep(1)

    # sem.acquire returned True and the semaphore is acquired
    print("{.name}: using resource.".format(c_thread))
    time.sleep(0.5)
    sem.release()


# How many threads can access the resource at the same time
limit = 2

# Create the semaphore
semaphore = threading.BoundedSemaphore(limit)

# Create more threads than the limit
ths = []
for _ in range(2 * limit):
    ths.append(threading.Thread(
        target=use_limited_resource,
        args=(semaphore, ))
    )
    ths[-1].start()

# Wait for termination
for th in ths:
    th.join()
