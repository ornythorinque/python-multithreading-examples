# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""Simple script showing how to use Condition object."""
import threading
import time


# The shared slot
slot = None


def producer(cond, n=5):
    """
    Produce n items and put them in the shared slot.

    This is an artificial example and the function will
    wait 1sec between each production to let the consumer
    consume the item.
    """
    global slot
    for i in range(n):
        with cond:
            slot = 'item_{}'.format(i)
            cond.notify()
        time.sleep(1)


def consumer(cond, n=5):
    """
    Consume n items when available.

    It uses the condition to know when the item is available
    and it will print it to make sure it is the right one.
    """
    global slot
    for _ in range(n):
        with cond:
            cond.wait_for(lambda: slot is not None)
            print('Item is ' + slot)
            slot = None


# Create condition
condition = threading.Condition()

# Create producer and consumer
prod = threading.Thread(target=producer, args=(condition, ))
cons = threading.Thread(target=consumer, args=(condition, ))

# Start them
prod.start()
cons.start()

# Wait for termination
prod.join()
cons.join()
