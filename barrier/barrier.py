# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""Siple script to show how to use Barrier object."""
import threading


def target_func(barrier):
    """Is a simple body for our threads."""
    barrier.wait()
    print("{.name} ended.".format(threading.current_thread()))


# How many thread to wait before release
limit = 2

# Create Barrier
ba = threading.Barrier(
    parties=limit,
    action=lambda: print("{} threads released !".format(limit)))

# Create thread and launch them
ths = []
for _ in range(2 * limit):
    ths.append(threading.Thread(target=target_func, args=(ba, )))
    ths[-1].start()

# Wait for termination
for th in ths:
    th.join()
