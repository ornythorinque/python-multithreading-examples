# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""A simple script to demonstrate RLock functionality."""
import threading


def target_func(lock, ntimes=5):
    """Acquire multiple times the same rlock and release it."""
    c_thread = threading.current_thread()
    for i in range(ntimes):
        lock.acquire()
        print("{.name} acquired the lock ({})".format(c_thread, i+1))

    for i in range(ntimes):
        lock.release()


# Create RLock
lock = threading.RLock()

# Create thread and launch it
th = threading.Thread(target=target_func, args=(lock, ))
th.start()

# Wait for termination
th.join()
