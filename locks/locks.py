# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""A simple script to demonstrate how Lock and RLock work."""
import threading


def acquire_lock(lock):
    """Acquire lock."""
    lock.acquire()
    print("Thread {.name} acquired lock".format(threading.current_thread()))


def release_lock(lock):
    """Release lock."""
    lock.release()
    print("Thread {.name} released lock".format(threading.current_thread()))


def acquire_and_release_lock(lock):
    """Acquire and release the lock."""
    lock.acquire()
    print("Thread {.name} acquired lock".format(threading.current_thread()))
    lock.release()
    print("Thread {.name} released lock".format(threading.current_thread()))


if __name__ == "__main__":
    import time

    # Create lock
    lock1 = threading.Lock()

    # Create threads
    th1 = threading.Thread(target=acquire_lock, args=(lock1, ))
    th2 = threading.Thread(target=acquire_and_release_lock, args=(lock1, ))
    th3 = threading.Thread(target=release_lock, args=(lock1, ))

    # Start them. Sleep a while between launches
    th1.start()
    time.sleep(0.1)

    th2.start()
    time.sleep(0.1)

    th3.start()

    # Wait thread termination
    th1.join()
    th2.join()
    th3.join()
