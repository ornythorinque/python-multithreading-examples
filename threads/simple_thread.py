# -*- coding: utf-8 -*-
#! /usr.bin/env python3
""" A simple script to demonstrate how to create a Thread and execute a function on it"""
import threading


def my_func(a, b):
    """
    Add b to a and return the result.

    This function also prints the name of the current thread.

    :param a: first operand
    :param b: second operand
    """
    print('Function executed on {}'.format(threading.current_thread()))
    return a + b


# Print the main thread
print("Current thread is {}".format(threading.current_thread()))

# Create a new Thread
# target argument is our custom function
# args is an iterable containing our function arguments
th = threading.Thread(target=my_func, args=(3, 4))

# Start the Thread
th.start()

# Print current thread again
print("Current thread is {}".format(threading.current_thread()))

# Don't forget to join the Thread if it is not a daemon !
th.join()
