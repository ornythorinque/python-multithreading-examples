# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""This script shows how to subclass the Thread class."""
import threading
import time
import datetime


class MyThread(threading.Thread):
    """
    A customized Thread that will wait the given time before calling its run method.

    It shows how to correctly override the constructor. In fact, the threading module
    contains a Timer class that does what our custom class is doing.
    """

    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, delay=0):
        """
        Create a new MyThread object.

        All arguments are the same as defined for Thread, except the delay
        one. If set, it must be a positive number (float or int) and it
        will delay the target's call. Default is 0.
        """
        super().__init__(group, target, name, args, kwargs)
        if delay < 0:
            raise ValueError()
        self.delay = delay

    def run(self):
        """Call the target with a delay (if set)."""
        print("[{}] Before delay".format(datetime.datetime.now()))
        time.sleep(self.delay)
        print("[{}] Calling target now".format(datetime.datetime.now()))
        super().run()


if __name__ == "__main__":
    # Create and start thread
    th = MyThread(target=lambda: print("Hello world !"), delay=1)
    th.start()

    # Wait for termination
    th.join()
