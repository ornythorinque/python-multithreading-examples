# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""Simple script to show how to use Event object."""
import threading
import time


def top(event):
    """Set the GO."""
    print("{.name} says GO !".format(threading.current_thread()))
    event.set()


def wait_top(event):
    """Wait for GO."""
    print("{.name} is waiting.".format(threading.current_thread()))
    event.wait()
    print("{.name} GO received.".format(threading.current_thread()))


# Create event
evt = threading.Event()

# Create threads
th1 = threading.Thread(target=top, args=(evt, ))
th2 = threading.Thread(target=wait_top, args=(evt, ))
th3 = threading.Thread(target=wait_top, args=(evt, ))

# Start them
th2.start()
th3.start()
time.sleep(1)

th1.start()

# Wait for termination
th1.join()
th2.join()
th3.join()
