# -*- coding: utf-8 -*-
#! /usr/bin/env python3
"""This script show how to use the Queue object to communicate between threads."""
import queue
import time
import contextlib
import threading


def producer(queue, n=10):
    """Produce n items and put them in the queue."""
    c_thread = threading.current_thread()
    for i in range(n):
        print("{.name} produced item_{}".format(c_thread, i+1))
        queue.put('item_{}'.format(i+1))


def consumer(in_queue):
    """Consume until the queue is empty."""
    c_thread = threading.current_thread()
    with contextlib.suppress(queue.Empty):
        while True:
            print("{.name} consumed {}".format(
                c_thread, in_queue.get(block=False)))
            in_queue.task_done()


# Queue's capacity
capacity = 3

# Create queue
in_q = queue.Queue(capacity)

# Create threads
prod = threading.Thread(target=producer, args=(in_q, 2*capacity))
cons1 = threading.Thread(target=consumer, args=(in_q, ))
cons2 = threading.Thread(target=consumer, args=(in_q, ))

# Start threads
prod.start()
time.sleep(0.1)

cons1.start()
cons2.start()

# Wait for termination
prod.join()
cons1.join()
cons2.join()
